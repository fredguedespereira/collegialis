# Collegialis#

Sistema de Gestão de Colegiados do IFPB.

### Objetivos Gerais do Collegialis ###

* Criar e gerenciar colegiados (e seus membros) de um curso superior.
* Recepcionar e distribui processos para um relator-membro de um colegiado.
* Planejar reuniões e definir suas pautas.
* Permitir aos relatores consultar processos e reuniões, bem como redigir pareceres para processos.
* Secretariar uma reunião de colegiado, registrando votos e gerando atas.
* Gerenciar cadastros diversos de usuários, cursos, professores, alunos e outros.

### Tecnologias ###

O Collegialis é um software web desenvolvido como projeto do curso de Programação para Web II do Curso Superior de TSI do IFPB. As seguintes tecnologias
são utilizadas:

* JavaServer Faces 2.2
* JPA 2.0 e Hibernate (provider de persistência)
* Apache Tomcat 8.0 (container)
* Bootstrap (GUI)
* Primefaces (GUI JSF)
* Bootsface (biblioteca de componentes JSF-Bootstrap)
* Gentelella - Bootstrap Admin Template by Colorlib

### Contribuição ###

Professores:

* Frederico Costa Guedes Pereira
* Valéria Cavalcanti

Alunos:

* ANTONIO DE PÁDUA PALITOT JÚNIOR
* DIEGO CARVALHO DO NASCIMENTO
* ERLYSON LUCENA DUARTE PEREIRA
* JOÃO PAULO MARQUES DE ALMEIDA
* LUCAS ALVES SCHULZE
* MATHEUS MAYER DUARTE DE FIGUEIRÊDO
* PEDRO VINÍCIUS SILVA DE PAIVA
* RAPHAEL SOARES DE CARVALHO
* SAMYRA LARA FERREIRA DE ALMEIDA
* SERGIO CAMPOS DA SILVA
* TIAGO CESÁRIO BARBOSA
* TULLIO HENRIQUE SOARES GOMES
* WASHINGTON BRUNO RODRIGUES CAVALCANTE


### Contatos ###

* fredguedespereira@gmail.com