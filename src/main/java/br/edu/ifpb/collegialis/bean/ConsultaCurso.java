package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.DAOException;
import br.edu.ifpb.collegialis.dao.CursoDAO;
import br.edu.ifpb.collegialis.entity.Coordenador;
import br.edu.ifpb.collegialis.entity.Curso;

@Named
@ViewScoped
public class ConsultaCurso extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ConsultaCurso.class);

	private List<Curso> cursos;

	@Inject
	private CursoDAO dao;
	
	@PostConstruct
	private void init() {
		this.popularListaDeCursoes();
	}
	
	public String editar(Curso cursoSelecionado) {
		cursoSelecionado = dao.find(cursoSelecionado.getId());  //recupera a id de banco do objeto
		this.putFlash("curso", cursoSelecionado);
		return "/curso/cadastro?faces-redirect=true";
	}
	
	public String criarCoord(Curso cursoSelecionado) {
		cursoSelecionado = dao.find(cursoSelecionado.getId());  //recupera a id de banco do objeto
		this.putFlash("curso", cursoSelecionado);
		return "/curso/cadcoord?faces-redirect=true";
	}
	
	public String mostrarHistCoord(Curso cursoSelecionado) {
		List<Coordenador> historico = dao.selectHistorico(cursoSelecionado);
		this.putFlash("historico", historico);
		return "/curso/histcoords?faces-redirect=true";
	}


	public void popularListaDeCursoes() {
		try {
			this.cursos = dao.findAll();
		} catch (DAOException e) {
			this.addErrorMessage("N�o foi poss�vel obter todos os alunos");
			logger.error("Erro ao obter todos os alunos", e);
		} 
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

}
