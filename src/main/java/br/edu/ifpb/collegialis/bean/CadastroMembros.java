package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.AlunoDAO;
import br.edu.ifpb.collegialis.dao.ColegiadoDAO;
import br.edu.ifpb.collegialis.dao.MembroDAO;
import br.edu.ifpb.collegialis.dao.ProfessorDAO;
import br.edu.ifpb.collegialis.entity.Aluno;
import br.edu.ifpb.collegialis.entity.Colegiado;
import br.edu.ifpb.collegialis.entity.Membro;
import br.edu.ifpb.collegialis.entity.Professor;
import br.edu.ifpb.collegialis.jpa.Transactional;
import br.edu.ifpb.collegialis.type.TipoMembro;

@Named 
@ViewScoped
public class CadastroMembros extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CadastroMembros.class);

	@Inject
	private MembroDAO daoMembro;
	
	@Inject
	private AlunoDAO daoAluno;
	
	@Inject
	private ProfessorDAO daoProfessor;
	
	@Inject
	private ColegiadoDAO daoColegiado;
	
	private Membro membro;
	
	private String tituloForm;
	
	private Colegiado colegiado;
	
	// Autocomplete
	private String nomeAlunoSelecionado;
	private List<Aluno> listaAlunosTemp;
	private String nomeProfessorSelecionado;
	private List<Professor> listaProfessoresTemp;

	@PostConstruct
	private void init() {
		this.colegiado = (Colegiado) this.getFlash("colegiado");
		this.colegiado = daoColegiado.update(this.colegiado);
		Membro membro = (Membro) this.getFlash("membro");
		if (membro != null) {
			this.membro = membro;
			this.tituloForm = "Modifica membro de colegiado";
			this.clearFlash();
		} else {
			this.membro = new Membro();
			this.membro.setTipo(TipoMembro.PROFESSOR);
			this.tituloForm = "Novo membro de colegiado";
		}
	}
	
	public List<String> autoCompleteNomeAluno(String nome) {
		List<String> listaNomes = new ArrayList<String>();
		listaAlunosTemp = new ArrayList<Aluno>();

		List<Aluno> listaAlunosEncontrados = null;

		try {
			listaAlunosEncontrados = daoAluno.consultaPorNome(nome + "%");
		} catch (Exception e) {
			logger.error(e.getStackTrace());
		}

		for (Aluno aluno : listaAlunosEncontrados) {
			listaNomes.add(aluno.getNome());
			listaAlunosTemp.add(aluno);
		}

		return listaNomes;
	}
	
	public List<String> autoCompleteNomeProfessor(String nome) {
		List<String> listaNomes = new ArrayList<String>();
		listaProfessoresTemp = new ArrayList<Professor>();

		List<Professor> listaProfessoresEncontrados = null;

		try {
			listaProfessoresEncontrados = daoProfessor.consultaPorNome(nome + "%");
		} catch (Exception e) {
			logger.error(e.getStackTrace());
		}

		for (Professor professor : listaProfessoresEncontrados) {
			listaNomes.add(professor.getNome());
			listaProfessoresTemp.add(professor);
		}

		return listaNomes;
	}
	
	public void mudeTipo(AjaxBehaviorEvent e) {
	}
	
	@Transactional
	public String excluir(Membro m) {
		this.colegiado = daoColegiado.update(this.colegiado);
		this.colegiado.getMembros().remove(m);
		m.setColegiado(null);
		daoColegiado.update(this.colegiado);
		return null;
	}
	
	private Aluno findAlunoSelecionado() {
		for (Aluno aluno : this.listaAlunosTemp) {
			if (aluno.getNome().equalsIgnoreCase(this.nomeAlunoSelecionado)) {
				return aluno;
			}
		}
		return null;
	}
	
	private Professor findProfessorSelecionado() {
		for (Professor professor : this.listaProfessoresTemp) {
			if (professor.getNome().equalsIgnoreCase(this.nomeProfessorSelecionado)) {
				return daoProfessor.update(professor);
			}
		}
		return null;
	}

	@Transactional
	public String salvar() {
		String proxPagina = null;
		String operacao = null;
		
		this.colegiado = daoColegiado.update(this.colegiado);  //reatacha objeto ao entitymanager
		if (this.membro.getTipo() == TipoMembro.ALUNO) {
			this.membro.setAluno(this.findAlunoSelecionado());
		} else {
			this.membro.setProfessor(this.findProfessorSelecionado());
		}
		try {
			if (this.membro.getId() == null) {
				this.colegiado.addMembro(this.membro);				
				daoColegiado.update(this.colegiado);
				operacao = "cadastrado";
			} else {
				daoMembro.update(this.membro);
				operacao = "atualizado";
			}
			proxPagina = null;
		} catch (PersistenceException e) {
			this.addErrorMessage("N�o foi poss�vel salvar o membro de colegiado" + this.membro.getNome() + ". Erro de banco de dados:"+e.getMessage());
			logger.error("Erro ao persistir membro de colegiado no banco", e);
		} 
		return proxPagina;
	}

	public String getTituloForm() {
		return tituloForm;
	}

	public void setTituloForm(String tituloForm) {
		this.tituloForm = tituloForm;
	}


	public Colegiado getColegiado() {
		return colegiado;
	}

	public void setColegiado(Colegiado colegiado) {
		this.colegiado = colegiado;
	}

	public Membro getMembro() {
		return membro;
	}

	public void setMembro(Membro membro) {
		this.membro = membro;
	}

	public String getNomeAlunoSelecionado() {
		return nomeAlunoSelecionado;
	}

	public void setNomeAlunoSelecionado(String nomeAlunoSelecionado) {
		this.nomeAlunoSelecionado = nomeAlunoSelecionado;
	}

	public String getNomeProfessorSelecionado() {
		return nomeProfessorSelecionado;
	}

	public void setNomeProfessorSelecionado(String nomeProfessorSelecionado) {
		this.nomeProfessorSelecionado = nomeProfessorSelecionado;
	}
	

}
