package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import br.edu.ifpb.collegialis.dao.UsuarioDAO;
import br.edu.ifpb.collegialis.entity.Usuario;
import br.edu.ifpb.collegialis.jpa.HSQLQualifier;

@Named
@SessionScoped
public class LoginBean extends GenericBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String usuario;
	
	private String senha;
	
	private Usuario logado;
	
	@Inject
	@HSQLQualifier
	EntityManager em;
	
	public String autentique() {
		this.usuario = this.usuario.trim();
		
		UsuarioDAO udao = new UsuarioDAO(em);
		Usuario u = udao.findByLogin(this.usuario);
		if(u.getEmail().equalsIgnoreCase(usuario) && u.getSenha().equals(senha)) {
			this.logado = u;
			this.senha = null;
			return "/principal?faces-redirect=true";
		} else {
			this.addErrorMessage("Login e/ou senha inv�lido(s)!");
			return null;
		}
	}
	
	public String sair() {
		this.invalidateSession();
		return "/login/login.jsf?faces-redirect=true";
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getLogado() {
		return logado;
	}

	public void setLogado(Usuario logado) {
		this.logado = logado;
	}

}
