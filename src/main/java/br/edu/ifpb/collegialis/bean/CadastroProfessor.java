package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.ProfessorDAO;
import br.edu.ifpb.collegialis.entity.Professor;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named
@ViewScoped
public class CadastroProfessor extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CadastroProfessor.class);

	private Professor professor;

	@Inject
	private ProfessorDAO dao;
	
	private String tituloForm;

	@PostConstruct
	private void init() {
		Professor professor = (Professor) this.getFlash("professor");
		if (professor != null) {
			this.professor = professor;
			this.tituloForm = "Modifica professor";
			this.clearFlash();
		} else {
			this.professor = new Professor();
			this.tituloForm = "Novo professor";
		}
	}

	@Transactional
	public String salvar() {
		String proxPagina = null;
		String operacao = null;
		try {
			if (this.professor.getId() == null) {
				// Novo aluno
				dao.insert(this.professor);
				operacao = "inserido";
			} else {
				// Modifica aluno
				dao.update(this.professor);
				operacao = "atualizado";
			}
			this.keepMessagesFlash(true);	//passa a mensagem para a pr�xima view, por Flash
			this.addInfoMessage("Professor '" + this.professor.getNome() + "' " + operacao + " com sucesso!");
			proxPagina = "/professor/consulta?faces-redirect=true";
		} catch (PersistenceException e) {
			this.addErrorMessage("N�o foi poss�vel salvar o professor " + this.professor.getNome() + ". Erro de banco de dados:"+e.getMessage());
			logger.error("Erro ao inserir professor no banco", e);
		} 
		return proxPagina;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public String getTituloForm() {
		return tituloForm;
	}

	public void setTituloForm(String tituloForm) {
		this.tituloForm = tituloForm;
	}
	

}
