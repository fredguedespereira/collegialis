package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.AlunoDAO;
import br.edu.ifpb.collegialis.dao.DAOException;
import br.edu.ifpb.collegialis.entity.Aluno;

@Named
@ViewScoped
public class ConsultaAluno extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ConsultaAluno.class);

	private List<Aluno> alunos;

	@Inject
	private AlunoDAO dao;

	@PostConstruct
	private void init() {
		this.popularListaDeAlunos();
	}
	
	public String editar(Aluno alunoSelecionado) {
		alunoSelecionado = dao.find(alunoSelecionado.getId());  //recupera a id de banco do objeto
		this.putFlash("aluno", alunoSelecionado);
		return "/aluno/cadastro?faces-redirect=true";
	}

	public void popularListaDeAlunos() {
		try {
			this.alunos = dao.findAll();
		} catch (DAOException e) {
			this.addErrorMessage("N�o foi poss�vel obter todos os alunos");
			logger.error("Erro ao obter todos os alunos", e);
		} 
	}
	
	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

}
