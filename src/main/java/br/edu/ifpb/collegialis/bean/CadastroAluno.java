package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.AlunoDAO;
import br.edu.ifpb.collegialis.entity.Aluno;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named
@ViewScoped
public class CadastroAluno extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CadastroAluno.class);

	@Inject
	private AlunoDAO dao;

	private Aluno aluno;

	private String tituloForm;

	@PostConstruct
	private void init() {
		Aluno aluno = (Aluno) this.getFlash("aluno");
		if (aluno != null) {
			this.aluno = aluno;
			this.tituloForm = "Modifica aluno";
			this.clearFlash();
		} else {
			this.aluno = new Aluno();
			this.tituloForm = "Novo aluno";
		}
	}

	@Transactional
	public String salvar() {
		String proxPagina = null;
		String operacao = null;
		try {
			this.keepMessagesFlash(true);	//passa a mensagem para a pr�xima view, por Flash
			if (this.aluno.getId() == null) {
				// Novo aluno
				dao.insert(this.aluno);
				operacao = "inserido";
			} else {
				// Modifica aluno
				dao.update(this.aluno);
				operacao = "modificado";
			}
			this.addInfoMessage("Aluno '" + aluno.getNome() + "' " + operacao + " com sucesso!");
			proxPagina = "/aluno/consulta?faces-redirect=true";
		} catch (PersistenceException e) {
			this.addErrorMessage("N�o foi poss�vel salvar o aluno "+aluno.getNome()+". Erro de banco de dados:"+e.getMessage());
			logger.error("Erro ao inserir aluno no banco", e);
		} 
		return proxPagina;
	}
	
	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getTituloForm() {
		return tituloForm;
	}

	public void setTituloForm(String tituloForm) {
		this.tituloForm = tituloForm;
	}

}
