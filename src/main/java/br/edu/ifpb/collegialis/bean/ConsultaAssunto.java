package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

import br.edu.ifpb.collegialis.dao.AssuntoDAO;
import br.edu.ifpb.collegialis.dao.DAOException;
import br.edu.ifpb.collegialis.entity.Assunto;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named
@ViewScoped
public class ConsultaAssunto extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ConsultaAssunto.class);

	private List<Assunto> assuntos;

	@Inject
	private AssuntoDAO daoAssunto;

	@PostConstruct
	private void init() {
		this.popularListaDeAssuntos();
	}
	
	@Transactional
	public void editaLinha(RowEditEvent event) {
		Assunto editado = (Assunto) event.getObject();
		try {
			daoAssunto.update(editado);
		} catch(DAOException e) {
			this.addErrorMessage("Problemas ao tentar salvar assunto! Erro: "+e.getMessage());
		}
    }
     
    public void cancelaEditaLinha(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edi��o do assunto '" + ((Assunto) event.getObject()).getDescricao() + "' cancelada!");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
	public String editar(Assunto assuntoSelecionado) {
		assuntoSelecionado = daoAssunto.find(assuntoSelecionado.getId());  //recupera a id de banco do objeto
		this.putFlash("assunto", assuntoSelecionado);
		return "/assunto/cadastro?faces-redirect=true";
	}

	public void popularListaDeAssuntos() {
		try {
			this.assuntos = daoAssunto.findAll();
		} catch (DAOException e) {
			this.addErrorMessage("N�o foi poss�vel obter todos os assuntos");
			logger.error("Erro ao obter todos os assuntos", e);
		} 
	}

	public List<Assunto> getAssuntos() {
		return assuntos;
	}

	public void setAssuntos(List<Assunto> assuntos) {
		this.assuntos = assuntos;
	}
	

}
