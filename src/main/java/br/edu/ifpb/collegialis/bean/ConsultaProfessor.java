package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.DAOException;
import br.edu.ifpb.collegialis.dao.ProfessorDAO;
import br.edu.ifpb.collegialis.entity.Colegiado;
import br.edu.ifpb.collegialis.entity.Curso;
import br.edu.ifpb.collegialis.entity.Professor;

@Named
@ViewScoped
public class ConsultaProfessor extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ConsultaProfessor.class);

	private List<Professor> professores;
	
	private Professor filtroProfessor;

	@Inject
	private ProfessorDAO dao;
	
	@PostConstruct
	private void init() {
		this.filtroProfessor = new Professor();
		this.popularListaDeProfessores();
	}
	
	public String editar(Professor professorSelecionado) {
		professorSelecionado = dao.find(professorSelecionado.getId());  //recupera a id de banco do objeto
		this.putFlash("professor", professorSelecionado);
		return "/professor/cadastro?faces-redirect=true";
	}

	public void popularListaDeProfessores() {
		try {
			this.professores = dao.findAll(filtroProfessor);
		} catch (DAOException e) {
			this.addErrorMessage("N�o foi poss�vel obter todos os alunos");
			logger.error("Erro ao obter todos os alunos", e);
		} 
	}
	
	public void filtrarProfessores(ValueChangeEvent e) {
		this.filtroProfessor.setCurso((Curso) e.getNewValue());
		this.popularListaDeProfessores();
	}

	public List<Professor> getProfessores() {
		return professores;
	}

	public void setProfessores(List<Professor> professores) {
		this.professores = professores;
	}


	public Professor getFiltroProfessor() {
		return filtroProfessor;
	}

	public void setFiltroProfessor(Professor filtroProfessor) {
		this.filtroProfessor = filtroProfessor;
	}

}
