package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.ColegiadoDAO;
import br.edu.ifpb.collegialis.dao.CoordenadorDAO;
import br.edu.ifpb.collegialis.dao.CursoDAO;
import br.edu.ifpb.collegialis.entity.Colegiado;
import br.edu.ifpb.collegialis.entity.Coordenador;
import br.edu.ifpb.collegialis.entity.Curso;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named 
@ViewScoped
public class CadastroCurso extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CadastroCurso.class);

	@Inject
	private CursoDAO daoCurso;
	
	@Inject
	private CoordenadorDAO daoCoordenador;
	
	@Inject
	private ColegiadoDAO daoColegiado;
	
	private Curso curso;
	
	private String tituloForm;
	
	@PostConstruct
	private void init() {
		Curso curso = (Curso) this.getFlash("curso");
		if (curso != null) {
			this.curso = curso;
			this.tituloForm = "Modifica curso";
			this.clearFlash();
		} else {
			this.curso = new Curso();
			this.tituloForm = "Novo curso";
		}
	}

	@Transactional
	public String salvar() {
		String proxPagina = null;
		String operacao = null;
		try {
			/* Quando o coordenador vai para o JSF, perde o contexto de persist�ncia e as associa��es precisam ser refeitas */
			if (this.curso.getCoordenador() != null) {
				Coordenador coord = daoCoordenador.find(this.curso.getCoordenador().getId());  //coordenador est� detached quando um novo curso � criado
				coord.setCurso(this.curso);  //relacionamento bidirecional
				this.curso.setCoordenador(coord);
			}
			
			/* Mesmo para o colegiado (outro dos campos que podem ser definidos no cadastro do curso */
			if (this.curso.getColegiado() != null) {
				Colegiado colegiado = daoColegiado.find(this.curso.getColegiado().getId());  //coordenador est� detached quando um novo curso � criado
				colegiado.setCurso(this.curso);  //relacionamento bidirecional
				this.curso.setColegiado(colegiado);
			}
			/* Decide se vai inserir ou atualiar o curso */
			if (this.curso.getId() == null) {
				daoCurso.insert(this.curso);
				operacao = "inserido";
			} else {
				daoCurso.update(this.curso);
				operacao = "modificado";
			}
			this.keepMessagesFlash(true);	//passa a mensagem para a pr�xima view, por Flash
			this.addInfoMessage("Curso '" + this.curso.getNome() + "' " + operacao + " com sucesso!");
			proxPagina = "/curso/consulta?faces-redirect=true";
		} catch (PersistenceException e) {
			daoCurso.rollback();
			this.addErrorMessage("N�o foi poss�vel salvar o curso " + this.curso.getNome() + ". Erro de banco de dados:"+e.getMessage());
			logger.error("Erro ao inserir curso no banco", e);
		} 
		return proxPagina;
	}

	public String getTituloForm() {
		return tituloForm;
	}

	public void setTituloForm(String tituloForm) {
		this.tituloForm = tituloForm;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
}
