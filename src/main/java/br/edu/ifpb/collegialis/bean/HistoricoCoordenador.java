package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.edu.ifpb.collegialis.dao.CoordenadorDAO;
import br.edu.ifpb.collegialis.entity.Coordenador;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named
@ViewScoped
public class HistoricoCoordenador extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Coordenador> historico;

	@Inject
	private CoordenadorDAO dao;
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	private void init() {
		historico = (List<Coordenador>) this.getFlash("historico");
	}
	
	@Transactional
	public String excluir(Coordenador coordenador) {
		dao.delete(coordenador);
		this.putFlash("historico", historico);
		return "/curso/histcoords?faces-redirect=true";
	}
	
	public List<Coordenador> getHistorico() {
		return historico;
	}

	public void setHistorico(List<Coordenador> historico) {
		this.historico = historico;
	}

}
