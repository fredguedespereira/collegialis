package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.ColegiadoDAO;
import br.edu.ifpb.collegialis.entity.Colegiado;
import br.edu.ifpb.collegialis.entity.Curso;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named 
@ViewScoped
public class CadastroColegiado extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CadastroColegiado.class);

	@Inject
	private ColegiadoDAO daoColegiado;
	
	private Curso curso;
	
	private String tituloForm;
	
	private Colegiado colegiado;

	@PostConstruct
	private void init() {
		Colegiado colegiado = (Colegiado) this.getFlash("colegiado");
		if (colegiado != null) {
			this.colegiado = colegiado;
			this.tituloForm = "Modifica colegiado";
			this.clearFlash();
		} else {
			this.colegiado = new Colegiado();
			this.tituloForm = "Novo colegiado";
		}
	}

	@Transactional
	public String salvar() {
		String proxPagina = null;
		String operacao = null;
		try {
			if (this.colegiado.getId() == null) {
				daoColegiado.insert(colegiado);
				operacao = "inserido";
			} else {
				daoColegiado.update(colegiado);
				operacao = "atualizado";
			}
			this.keepMessagesFlash(true);	//passa a mensagem para a pr�xima view, por Flash
			this.addInfoMessage("Colegiado '" + this.colegiado.getDescricao() + "' " + operacao + " com sucesso!");
			proxPagina = "/colegiado/consulta?faces-redirect=true";
		} catch (PersistenceException e) {
			this.addErrorMessage("N�o foi poss�vel salvar o colegiado" + this.colegiado.getDescricao() + ". Erro de banco de dados:"+e.getMessage());
			logger.error("Erro ao inserir colegiado no banco", e);
		} 
		return proxPagina;
	}

	public String getTituloForm() {
		return tituloForm;
	}

	public void setTituloForm(String tituloForm) {
		this.tituloForm = tituloForm;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Colegiado getColegiado() {
		return colegiado;
	}

	public void setColegiado(Colegiado colegiado) {
		this.colegiado = colegiado;
	}
	

}
