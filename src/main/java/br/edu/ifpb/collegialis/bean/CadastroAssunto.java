package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.AssuntoDAO;
import br.edu.ifpb.collegialis.entity.Assunto;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named 
@ViewScoped
public class CadastroAssunto extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CadastroAssunto.class);

	private Assunto assunto;

	@Inject
	private AssuntoDAO dao;

	@PostConstruct
	private void init() {
		this.assunto = new Assunto();
	}

	@Transactional
	public String salvar() {
		String proxPagina = null;
		try {
			dao.insert(assunto);
			this.addInfoMessage("Assunto '" + assunto.getDescricao() + "' salvo com sucesso!");
			this.keepMessagesFlash(true);
			proxPagina = "/assunto/consulta?faces-redirect=true";
		} catch (PersistenceException e) {
			this.addErrorMessage("N�o foi poss�vel salvar o assunto "+ assunto.getDescricao() + ". Erro de banco de dados:"+e.getMessage());
			logger.error("Erro ao inserir aluno no banco", e);
		} 
		return proxPagina;
	}

	public Assunto getAssunto() {
		return assunto;
	}

	public void setAssunto(Assunto assunto) {
		this.assunto = assunto;
	}


}
