package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.CoordenadorDAO;
import br.edu.ifpb.collegialis.dao.DAOException;
import br.edu.ifpb.collegialis.entity.Coordenador;

@Named
@ViewScoped
public class ConsultaCoordenador extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ConsultaCoordenador.class);

	private List<Coordenador> coordenador;

	@Inject
	private CoordenadorDAO dao;
	
	@PostConstruct
	private void init() {
		this.popularListaDeCoordenadores();
	}
	
	public String editar(Coordenador coordenadorSelecionado) {
		coordenadorSelecionado = dao.find(coordenadorSelecionado.getId());  //recupera a id de banco do objeto
		this.putFlash("coordenador", coordenadorSelecionado);
		return "/coordenador/cadastro?faces-redirect=true";
	}
	
	public void popularListaDeCoordenadores() {
		try {
			this.coordenador = dao.findAll();
		} catch (DAOException e) {
			this.addErrorMessage("N�o foi poss�vel obter todos os coordenadores");
			logger.error("Erro ao obter todos os coordenadores", e);
		} 
	}

	public List<Coordenador> getCoordenadores() {
		return coordenador;
	}

	public void setCoordenadores(List<Coordenador> coordenadores) {
		this.coordenador = coordenadores;
	}

}
