package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.ColegiadoDAO;
import br.edu.ifpb.collegialis.dao.DAOException;
import br.edu.ifpb.collegialis.entity.Colegiado;
import br.edu.ifpb.collegialis.entity.Curso;
import br.edu.ifpb.collegialis.entity.Membro;

@Named
@ViewScoped
public class ConsultaColegiado extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ConsultaColegiado.class);

	private List<Colegiado> colegiados;
	
	private Colegiado colegiadoSelecionado;
	
	private Colegiado filtroColegiado;
	 
	private boolean filtroAtivos = true;

	@Inject
	private ColegiadoDAO dao;
	
	@PostConstruct
	private void init() {
		filtroColegiado = new Colegiado();
		this.popularListaDeColegiados();
		this.colegiadoSelecionado = null;
	}
	
	public void filtrarAtivos() {
		this.popularListaDeColegiados();
	}
	
	public void filtrarColegiados(ValueChangeEvent e) {
		this.filtroColegiado.setCurso((Curso) e.getNewValue());
		this.popularListaDeColegiados();
	}
	
	public String editar(Colegiado colegiadoSelecionado) {
		colegiadoSelecionado = dao.find(colegiadoSelecionado.getId());  //recupera a id de banco do objeto
		this.putFlash("colegiado", colegiadoSelecionado);
		return "/colegiado/cadastro?faces-redirect=true";
	}
	
	public String cadMembros(Colegiado colegiadoSelecionado) {
		this.colegiadoSelecionado = dao.find(colegiadoSelecionado.getId());
		this.putFlash("colegiado", colegiadoSelecionado);
		return "/colegiado/cadmembros?faces-redirect=true";
	}
	
	public void popularListaDeColegiados() {
		try {
			this.colegiados = dao.findAll(filtroColegiado, filtroAtivos);
		} catch (DAOException e) {
			this.addErrorMessage("N�o foi poss�vel obter todos os colegiados");
			logger.error("Erro ao obter todos os colegiados", e);
		} 
	}

	public List<Colegiado> getColegiados() {
		return colegiados;
	}

	public void setColegiados(List<Colegiado> colegiados) {
		this.colegiados = colegiados;
	}

	public Colegiado getColegiadoSelecionado() {
		return colegiadoSelecionado;
	}

	public void setColegiadoSelecionado(Colegiado colegiadoSelecionado) {
		this.colegiadoSelecionado = colegiadoSelecionado;
	}

	public boolean isFiltroAtivos() {
		return filtroAtivos;
	}

	public void setFiltroAtivos(boolean filtroAtivos) {
		this.filtroAtivos = filtroAtivos;
	}

	public Colegiado getFiltroColegiado() {
		return filtroColegiado;
	}

	public void setFiltroColegiado(Colegiado filtroColegiado) {
		this.filtroColegiado = filtroColegiado;
	}



}
