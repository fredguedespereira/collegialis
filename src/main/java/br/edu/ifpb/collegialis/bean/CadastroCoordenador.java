package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import br.edu.ifpb.collegialis.dao.CoordenadorDAO;
import br.edu.ifpb.collegialis.dao.CursoDAO;
import br.edu.ifpb.collegialis.dao.ProfessorDAO;
import br.edu.ifpb.collegialis.entity.Coordenador;
import br.edu.ifpb.collegialis.entity.Curso;
import br.edu.ifpb.collegialis.entity.Professor;
import br.edu.ifpb.collegialis.jpa.Transactional;

@Named
@ViewScoped
public class CadastroCoordenador extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CadastroCoordenador.class);

	@Inject
	private CursoDAO daoCurso;
	
	@Inject
	private ProfessorDAO daoProfessor;

	@Inject
	private CoordenadorDAO daoCoordenador;

	private Curso curso;

	private Coordenador coordenador;

	private String tituloForm;
	
	@PostConstruct
	private void init() {
		this.curso = (Curso) this.getFlash("curso");
		this.coordenador = new Coordenador();
		this.tituloForm = "Novo coordenador";
		// }
	}
	
	public List<Professor> getProfessores(Curso curso) {
		return daoProfessor.findDoCurso(curso);
	}

	@Transactional
	public String salvar() {
		String proxPagina = null;
		try {

			daoCoordenador.insert(this.coordenador);
			this.coordenador.setCurso(this.curso);
			this.curso.setCoordenador(this.coordenador);
			daoCurso.update(curso);
			this.keepMessagesFlash(true); // passa a mensagem para a pr�xima
			this.addInfoMessage("Coordenador '" + this.coordenador.getNome() + "' cadastrado com sucesso no curso " + this.curso.getNome()+".");
			proxPagina = "/curso/consulta?faces-redirect=true";
		} catch (PersistenceException e) {
			this.addErrorMessage(
					"N�o foi poss�vel cadastrar o coordenador " + this.coordenador.getNome() + " no curso " + this.curso.getNome() + ". Erro de banco de dados:" + e.getMessage());
			logger.error("Erro ao cadastrar coordenador no banco", e);
		}
		return proxPagina;
	}

	public String getTituloForm() {
		return tituloForm;
	}

	public void setTituloForm(String tituloForm) {
		this.tituloForm = tituloForm;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Coordenador getCoordenador() {
		return coordenador;
	}

	public void setCoordenador(Coordenador coordenador) {
		this.coordenador = coordenador;
	}

}
