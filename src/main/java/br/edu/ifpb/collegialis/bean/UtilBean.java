package br.edu.ifpb.collegialis.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.edu.ifpb.collegialis.dao.ColegiadoDAO;
import br.edu.ifpb.collegialis.dao.CoordenadorDAO;
import br.edu.ifpb.collegialis.dao.CursoDAO;
import br.edu.ifpb.collegialis.dao.ProfessorDAO;
import br.edu.ifpb.collegialis.entity.Colegiado;
import br.edu.ifpb.collegialis.entity.Coordenador;
import br.edu.ifpb.collegialis.entity.Curso;
import br.edu.ifpb.collegialis.entity.Professor;
import br.edu.ifpb.collegialis.type.TipoMembro;

@Named
@ApplicationScoped
public class UtilBean extends GenericBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private ColegiadoDAO daoColegiado;

	@Inject
	private CursoDAO daoCurso;
	
	@Inject
	private ProfessorDAO daoProfessor;

	@Inject
	private CoordenadorDAO daoCoordenador;

	private List<Colegiado> colegiadosItens;

	private List<Coordenador> coordenadoresItens;

	private List<Curso> cursosItens;
	
	private List<Professor> professoresItens;

	@PostConstruct
	private void init() {
		coordenadoresItens = daoCoordenador.findAll();
		colegiadosItens = daoColegiado.findAllAtivos();
		cursosItens = daoCurso.findAll();
		professoresItens = daoProfessor.findAll();
	}

	public List<Professor> getProfessoresItens() {
		return professoresItens;
	}

	public List<Colegiado> getColegiadosItens() {
		return colegiadosItens;
	}

	public List<Coordenador> getCoordenadoresItens() {
		return coordenadoresItens;
	}

	public List<Curso> getCursosItens() {
		return cursosItens;
	}

	public List<TipoMembro> getTipoMembroItens() {
		return Arrays.asList(TipoMembro.values());
	}

}
