package br.edu.ifpb.collegialis.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.edu.ifpb.collegialis.type.TipoPerfil;

@Entity
@Table(name="TB_USUARIO")
public class Usuario {
	@Id
	@Column(name="NU_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="DS_NOME")
	private String nome;
	
	@Column(name="DS_EMAIL")
	private String email;
	
	@Column(name="DS_SENHA")
	private String senha;
	
	@Column(name="ST_ATIVO")
	private Boolean ativo;
	
	@Column(name="TP_PERFIL")
	@Enumerated(EnumType.STRING)
	private TipoPerfil perfil;
	
	@OneToOne (cascade = {CascadeType.MERGE})
	@JoinColumn(name = "ID_MEMBRO", foreignKey=@ForeignKey(name = "FK_MEMBRO"))
	private Membro membro;
	
	public String getPrenome() {
		return  (this.nome != null) ? this.nome.split(" ")[0].trim() : null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Membro getMembro() {
		return membro;
	}

	public void setMembro(Membro membro) {
		this.membro = membro;
	}

	public TipoPerfil getPerfil() {
		return perfil;
	}

	public void setPerfil(TipoPerfil perfil) {
		this.perfil = perfil;
	}

}
