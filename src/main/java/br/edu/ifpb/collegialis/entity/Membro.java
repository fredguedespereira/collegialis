package br.edu.ifpb.collegialis.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.edu.ifpb.collegialis.type.TipoMembro;

@Entity
@Table(name="TB_MEMBRO")
public class Membro {
	@Id
	@Column (name="NU_ID")
	@GeneratedValue (strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column (name="TP_MEMBRO")
	private TipoMembro tipo;
	
	@OneToOne (cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, orphanRemoval=false)
	@JoinColumn (name="NU_ID_ALUNO", foreignKey=@ForeignKey(name = "FK_ALUNO"))
	private Aluno aluno;
	
	@OneToOne (cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, orphanRemoval=false)
	@JoinColumn (name="NU_ID_PROFESSOR", foreignKey=@ForeignKey(name = "FK_PROFESSOR"))
	private Professor professor;
	
	@ManyToOne
	@JoinColumn (name="NU_ID_COLEGIADO", foreignKey=@ForeignKey(name = "FK_COLEGIADO"))
	private Colegiado colegiado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Colegiado getColegiado() {
		return colegiado;
	}

	public void setColegiado(Colegiado colegiado) {
		this.colegiado = colegiado;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
		this.tipo = TipoMembro.ALUNO;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
		this.tipo = TipoMembro.PROFESSOR;
	}

	public TipoMembro getTipo() {
		return tipo;
	}

	public void setTipo(TipoMembro tipo) {
		this.tipo = tipo;
	}
	
	public String getNome() {
		if (this.tipo == TipoMembro.ALUNO) {
			return this.aluno.getNome();
		} else {
			return this.professor.getNome();
		}
	}
	
	public String getPapel() {
		if (this.tipo == TipoMembro.ALUNO) {
			return "Aluno";
		} else {
			return "Docente";
		}
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno == null) ? 0 : aluno.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((professor == null) ? 0 : professor.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Membro other = (Membro) obj;
		if (aluno == null) {
			if (other.aluno != null)
				return false;
		} else if (!aluno.equals(other.aluno))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (professor == null) {
			if (other.professor != null)
				return false;
		} else if (!professor.equals(other.professor))
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getNome()+"["+tipo+"]";
	}
	
	
}
