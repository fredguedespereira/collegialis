package br.edu.ifpb.collegialis.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;

import br.edu.ifpb.collegialis.entity.Membro;

public class MembroDAO extends GenericDAO<Membro, Integer> implements Serializable{
	private static final long serialVersionUID = 1L;

	public MembroDAO() {
		super();
	}

	public MembroDAO(EntityManager em) {
		super(em);
	}
	

}
