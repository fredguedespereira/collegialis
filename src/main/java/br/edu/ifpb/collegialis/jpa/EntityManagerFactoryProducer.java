package br.edu.ifpb.collegialis.jpa;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactoryProducer {

	public static final String PERSISTENCE_UNIT = "collegialis";

	@Produces
	@ApplicationScoped
	@HSQLQualifier
	public EntityManagerFactory create() {
		return Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);

	}

	
	public void destroy(@Disposes @HSQLQualifier EntityManagerFactory factory) {
		factory.close();
	}
}
