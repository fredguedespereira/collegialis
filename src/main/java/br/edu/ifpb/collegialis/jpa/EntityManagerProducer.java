package br.edu.ifpb.collegialis.jpa;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class EntityManagerProducer {
	// private static EntityManagerFactory factory;
	//
	// public EntityManagerProducer() {
	// factory = Persistence.createEntityManagerFactory("collegialis");
	// }
	//
	// @Produces
	// @RequestScoped
	// public EntityManager createEntityManager() {
	// return factory.createEntityManager();
	// }
	//
	// public void closeEntityManager(@Disposes EntityManager manager) {
	// manager.close();
	// }
	// @PersistenceContext(unitName = "collegialis")
	@Inject
	@HSQLQualifier
	private EntityManagerFactory entityManagerFactory;

	public EntityManagerProducer() {
	}

	@Produces
	@RequestScoped
	@HSQLQualifier
	public EntityManager createEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

	public void dispose(@Disposes @HSQLQualifier EntityManager  entityManager) {
		entityManager.close();
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

}
