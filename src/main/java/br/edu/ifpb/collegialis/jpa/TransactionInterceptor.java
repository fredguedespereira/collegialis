package br.edu.ifpb.collegialis.jpa;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

@Interceptor
@Transactional
public class TransactionInterceptor implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Inject
	@HSQLQualifier
	private EntityManager entityManager;

	@AroundInvoke
	public Object invoke(InvocationContext context) throws Exception {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			if (!transaction.isActive()) {
				// truque para fazer rollback no que j� passou
				// (sen�o, um futuro commit, confirmaria at� mesmo opera��es sem transa��o)
				transaction.begin();
				transaction.rollback();
				// taca-lhe pau!
				transaction.begin();
			}
			return context.proceed();
		} catch (Exception e) {
			System.out.println("Chamando transa��o no m�todo:" + e);
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
			if (transaction != null && transaction.isActive()) {
				transaction.commit();
			}
		}
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
