package br.edu.ifpb.collegialis.converter;

/**
 * Interface que marca quais entidades podem ser convertidas em itens do componente JSF SelectOneMenu.
 * @author fred
 *
 */
public interface Selectable {
	public Integer getId();
}
