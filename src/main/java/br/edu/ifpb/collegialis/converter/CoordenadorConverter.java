package br.edu.ifpb.collegialis.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import br.edu.ifpb.collegialis.entity.Coordenador;

@FacesConverter("CoordConverter") 
public class CoordenadorConverter extends SelectItemsBaseConverter {
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null && (value instanceof Coordenador))
			return ((Coordenador) value).getId().toString();
		else
			return null;
	}

}
