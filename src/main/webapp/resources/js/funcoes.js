
//********************************** MASCARAS *******************************************************

//formato: (xx)xxxx-xxxx (ddd + 8 digitos)
function formata_fone_ddd(obj) {
	switch (obj.value.length) {
		case 1:
			obj.value = "(" + obj.value;
			break;
		case 3:
			obj.value = obj.value + ")";
			break;	
		case 8:
		case 9:
			obj.value = obj.value + "-";
			break;	
	}
}

//formato: xxxx-xxxx (8 digitos)
function formata_fone(obj) {
	switch (obj.value.length) {
		case 4:
			obj.value = obj.value + "-";
			break;	
	}
}

//formato: xxxxx-xxxx (9 digitos)
function formata_fone_9_digitos(obj) {
	switch (obj.value.length) {
		case 5:
			obj.value = obj.value + "-";
			break;	
	}
}

//formato: dd/mm/aaaa
function formata_data(obj) {
	switch (obj.value.length) {
		case 2:
			obj.value = obj.value + "/";
			break;
		case 5:
			obj.value = obj.value + "/";
			break;
	}
}


function apenas_numeros(caracter) {
	 var nTecla = 0;
	 if (document.all) {
		 nTecla = caracter.keyCode;
	 } 
	 else {
		 nTecla = caracter.which;
	 }
	 if ((nTecla> 47 && nTecla <58)
		 || nTecla == 8 || nTecla == 127 //8 = backspace, 127 = dell
		 || nTecla == 0 || nTecla == 9  // 0 == Tab
		 || nTecla == 13) { // 13 == Enter
		 return true;
	 } 
	 else {
		 return false;
	 }
}

function mascara(o,f){
	v_obj=o;
	v_fun=f;
	setTimeout("execmascara()",1);
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value);
}


//mascara de telefone 8 ou 9 digitos. formato: "(dd) xxxxx-xxxx" OU "(dd) xxxx-xxxx"
function mtel(v) {
	v=v.replace(/\D/g,""); //Remove tudo o que nao eh digito
	v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parenteses em volta dos dois primeiros digitos
	if (v.length <= 14) {
		v=v.replace(/(\d)(\d{4})$/,"$1-$2"); //Coloca hifen entre o quarto e o quinto digitos
	} else {
		v=v.replace(/(\d)(\d{3})$/,"$1-$2"); //Coloca hifen entre o quinto e o sexto digitos
	}
	return v;
}


function soNumeros(v){
    return v.replace(/\D/g,"");
}

function telefone(v){
    v=v.replace(/\D/g,"");                //Remove tudo o que n�o � d�gito
    v=v.replace(/^(\d\d)(\d)/g,"($1) $2"); //Coloca par�nteses em volta dos dois primeiros d�gitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2");    //Coloca h�fen entre o quarto e o quinto d�gitos
    return v;
}


function cpf(v){
    v=v.replace(/\D/g,"");                    //Remove tudo o que n�o � d�gito
    v=v.replace(/(\d{3})(\d)/,"$1.$2");       //Coloca um ponto entre o terceiro e o quarto d�gitos
    v=v.replace(/(\d{3})(\d)/,"$1.$2");       //Coloca um ponto entre o terceiro e o quarto d�gitos de novo (para o segundo bloco de n�meros)
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2"); //Coloca um h�fen entre o terceiro e o quarto d�gitos
    return v;
}

function cnpj(v) {
    v=v.replace(/\D/g,"");                           //Remove tudo o que nao eh digito
    v=v.replace(/^(\d{2})(\d)/,"$1.$2");             //Coloca ponto entre o segundo e o terceiro digitos
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3"); //Coloca ponto entre o quinto e o sexto digitos
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2");           //Coloca uma barra entre o oitavo e o nono digitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2");              //Coloca um hifen depois do bloco de quatro digitos
    return v;
}

function cpfCnpj(v){							//mascara de cpf e cnpj no mesmo campo (testa pelo tamanho)
	
    if (v.length <= 14) { //CPF
    	v=v.replace(/\D/g,"");                    //Remove tudo o que n�o � d�gito
    	v=v.replace(/(\d{3})(\d)/,"$1.$2");       //Coloca um ponto entre o terceiro e o quarto d�gitos
        v=v.replace(/(\d{3})(\d)/,"$1.$2");       //Coloca um ponto entre o terceiro e o quarto d�gitos de novo (para o segundo bloco de n�meros)
        v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2"); //Coloca um h�fen entre o terceiro e o quarto d�gitos
 
    } else { //CNPJ
    	v=v.replace(/\D/g,"");                    		//Remove tudo o que n�o � d�gito
    	v=v.replace(/^(\d{2})(\d)/,"$1.$2");             //Coloca ponto entre o segundo e o terceiro digitos
        v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3"); //Coloca ponto entre o quinto e o sexto digitos
        v=v.replace(/\.(\d{3})(\d)/,".$1/$2");           //Coloca uma barra entre o oitavo e o nono digitos
        v=v.replace(/(\d{4})(\d)/,"$1-$2");              //Coloca um hifen depois do bloco de quatro digitos
    }
    return v
}

function cep(v) {
    v=v.replace(/D/g,"");                //Remove tudo o que n�o � d�gito
    v=v.replace(/^(\d{5})(\d)/,"$1-$2"); //Esse � t�o f�cil que n�o merece explica��es
    return v;
}

function formataValor(campo) {
	campo.value = filtraCampoValor(campo);
	vr = campo.value;
	tam = vr.length;
	if ( tam <= 2 ){ 
		campo.value = vr ; }
 	if ( (tam > 2) && (tam <= 5) ){
 		campo.value = vr.substr( 0, tam - 2 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 6) && (tam <= 8) ){
 		campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 9) && (tam <= 11) ){
 		campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 12) && (tam <= 14) ){
 		campo.value = vr.substr( 0, tam - 11 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ; }
 	if ( (tam >= 15) && (tam <= 18) ){
 		campo.value = vr.substr( 0, tam - 14 ) + '.' + vr.substr( tam - 14, 3 ) + '.' + vr.substr( tam - 11, 3 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + ',' + vr.substr( tam - 2, tam ) ;}
}

			
// limpa todos os caracteres especiais do campo solicitado
function filtraCampoValor(campo){
	var s = "";
	var cp = "";
	vr = campo.value;
	tam = vr.length;
	for (i = 0; i < tam ; i++) {  
		if ( (!isNaN(vr.substring(i, i + 1))) && vr.substring(i,i + 1) != "/" && vr.substring(i,i + 1) != "-" && vr.substring(i,i + 1) != "."  && vr.substring(i,i + 1) != "," )  {					 		
		 	s = s + vr.substring(i,i + 1);}
	}
	campo.value = s;
	return cp = campo.value
}


//*********************************** VALIDACOES *************************************************

//valida data
function ValidaData(data){
    exp = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
        
    if(!exp.test(data.value))
        alert("Data invalida.");
        
    return (exp.test(data.value));            
}


/**
 * @author M�rcio d'�vila
 * @version 1.03, 2004-2008
 * http://www.mhavila.com.br/topicos/web/cpf_cnpj.html
 *
 * Licenciado sob os termos da licen�a Creative Commons,
 * Atribui��o - Compartilhamento pela mesma licen�a 2.5:
 * http://creativecommons.org/licenses/by-sa/2.5/br/
 *
 * PROT�TIPOS:
 * m�todo String.lpad(int pSize, char pCharPad)
 * m�todo String.trim()
 *
 * String unformatNumber(String pNum)
 * String formatCpfCnpj(String pCpfCnpj, boolean pUseSepar, boolean pIsCnpj)
 * String dvCpfCnpj(String pEfetivo, boolean pIsCnpj)
 * boolean isCpf(String pCpf)
 * boolean isCnpj(String pCnpj)
 * boolean isCpfCnpj(String pCpfCnpj)
 */


var NUM_DIGITOS_CPF  = 11;
var NUM_DIGITOS_CNPJ = 14;
var NUM_DGT_CNPJ_BASE = 8;


/**
 * Adiciona m�todo lpad() � classe String.
 * Preenche a String � esquerda com o caractere fornecido,
 * at� que ela atinja o tamanho especificado.
 */
String.prototype.lpad = function(pSize, pCharPad)
{
	var str = this;
	var dif = pSize - str.length;
	var ch = String(pCharPad).charAt(0);
	for (; dif>0; dif--) str = ch + str;
	return (str);
} //String.lpad


/**
 * Adiciona m�todo trim() � classe String.
 * Elimina brancos no in�cio e fim da String.
 */
String.prototype.trim = function()
{
	return this.replace(/^\s*/, "").replace(/\s*$/, "");
} //String.trim


/**
 * Elimina caracteres de formata��o e zeros � esquerda da string
 * de n�mero fornecida.
 * @param String pNum
 *      String de n�mero fornecida para ser desformatada.
 * @return String de n�mero desformatada.
 */
function unformatNumber(pNum)
{
	return String(pNum).replace(/\D/g, "").replace(/^0+/, "");
} //unformatNumber


/**
 * Formata a string fornecida como CNPJ ou CPF, adicionando zeros
 * � esquerda se necess�rio e caracteres separadores, conforme solicitado.
 * @param String pCpfCnpj
 *      String fornecida para ser formatada.
 * @param boolean pUseSepar
 *      Indica se devem ser usados caracteres separadores (. - /).
 * @param boolean pIsCnpj
 *      Indica se a string fornecida � um CNPJ.
 *      Caso contr�rio, � CPF. Default = false (CPF).
 * @return String de CPF ou CNPJ devidamente formatada.
 */
function formatCpfCnpj(pCpfCnpj, pUseSepar, pIsCnpj)
{
	if (pIsCnpj==null) pIsCnpj = false;
	if (pUseSepar==null) pUseSepar = true;
	var maxDigitos = pIsCnpj? NUM_DIGITOS_CNPJ: NUM_DIGITOS_CPF;
	var numero = unformatNumber(pCpfCnpj);

	numero = numero.lpad(maxDigitos, '0');

	if (!pUseSepar) return numero;

	if (pIsCnpj)
	{
		reCnpj = /(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/;
		numero = numero.replace(reCnpj, "$1.$2.$3/$4-$5");
	}
	else
	{
		reCpf  = /(\d{3})(\d{3})(\d{3})(\d{2})$/;
		numero = numero.replace(reCpf, "$1.$2.$3-$4");
	}
	return numero;
} //formatCpfCnpj


/**
 * Calcula os 2 d�gitos verificadores para o n�mero-efetivo pEfetivo de
 * CNPJ (12 d�gitos) ou CPF (9 d�gitos) fornecido. pIsCnpj � booleano e
 * informa se o n�mero-efetivo fornecido � CNPJ (default = false).
 * @param String pEfetivo
 *      String do n�mero-efetivo (SEM d�gitos verificadores) de CNPJ ou CPF.
 * @param boolean pIsCnpj
 *      Indica se a string fornecida � de um CNPJ.
 *      Caso contr�rio, � CPF. Default = false (CPF).
 * @return String com os dois d�gitos verificadores.
 */
function dvCpfCnpj(pEfetivo, pIsCnpj)
{
	if (pIsCnpj==null) pIsCnpj = false;
	var i, j, k, soma, dv;
	var cicloPeso = pIsCnpj? NUM_DGT_CNPJ_BASE: NUM_DIGITOS_CPF;
	var maxDigitos = pIsCnpj? NUM_DIGITOS_CNPJ: NUM_DIGITOS_CPF;
	var calculado = formatCpfCnpj(pEfetivo + "00", false, pIsCnpj);
	calculado = calculado.substring(0, maxDigitos - 2);
	var result = "";

	for (j = 1; j <= 2; j++)
	{
		k = 2;
		soma = 0;
		for (i = calculado.length-1; i >= 0; i--)
		{
			soma += (calculado.charAt(i) - '0') * k;
			k = (k-1) % cicloPeso + 2;
		}
		dv = 11 - soma % 11;
		if (dv > 9) dv = 0;
		calculado += dv;
		result += dv
	}

	return result;
} //dvCpfCnpj


/**
 * Testa se a String pCpf fornecida � um CPF v�lido.
 * Qualquer formata��o que n�o seja algarismos � desconsiderada.
 * @param String pCpf
 *      String fornecida para ser testada.
 * @return <code>true</code> se a String fornecida for um CPF v�lido.
 */
function isCpf(pCpf)
{
	var numero = formatCpfCnpj(pCpf, false, false);
	if (numero.length > NUM_DIGITOS_CPF) return false;

	var base = numero.substring(0, numero.length - 2);
	var digitos = dvCpfCnpj(base, false);
	var algUnico, i;

	// Valida d�gitos verificadores
	if (numero != "" + base + digitos) return false;

	/* N�o ser�o considerados v�lidos os seguintes CPF:
	 * 000.000.000-00, 111.111.111-11, 222.222.222-22, 333.333.333-33, 444.444.444-44,
	 * 555.555.555-55, 666.666.666-66, 777.777.777-77, 888.888.888-88, 999.999.999-99.
	 */
	algUnico = true;
	for (i=1; algUnico && i<NUM_DIGITOS_CPF; i++)
	{
		algUnico = (numero.charAt(i-1) == numero.charAt(i));
	}
	return (!algUnico);
} //isCpf


/**
 * Testa se a String pCnpj fornecida � um CNPJ v�lido.
 * Qualquer formata��o que n�o seja algarismos � desconsiderada.
 * @param String pCnpj
 *      String fornecida para ser testada.
 * @return <code>true</code> se a String fornecida for um CNPJ v�lido.
 */
function isCnpj(pCnpj)
{
	var numero = formatCpfCnpj(pCnpj, false, true);
	if (numero.length > NUM_DIGITOS_CNPJ) return false;

	var base = numero.substring(0, NUM_DGT_CNPJ_BASE);
	var ordem = numero.substring(NUM_DGT_CNPJ_BASE, 12);
	var digitos = dvCpfCnpj(base + ordem, true);
	var algUnico;

	// Valida d�gitos verificadores
	if (numero != "" + base + ordem + digitos) return false;

	/* N�o ser�o considerados v�lidos os CNPJ com os seguintes n�meros B�SICOS:
	 * 11.111.111, 22.222.222, 33.333.333, 44.444.444, 55.555.555,
	 * 66.666.666, 77.777.777, 88.888.888, 99.999.999.
	 */
	algUnico = numero.charAt(0) != '0';
	for (i=1; algUnico && i<NUM_DGT_CNPJ_BASE; i++)
	{
		algUnico = (numero.charAt(i-1) == numero.charAt(i));
	}
	if (algUnico) return false;

	/* N�o ser� considerado v�lido CNPJ com n�mero de ORDEM igual a 0000.
	 * N�o ser� considerado v�lido CNPJ com n�mero de ORDEM maior do que 0300
	 * e com as tr�s primeiras posi��es do n�mero B�SICO com 000 (zeros).
	 * Esta cr�tica n�o ser� feita quando o no B�SICO do CNPJ for igual a 00.000.000.
	 */
	if (ordem == "0000") return false;
	return (base == "00000000"
		|| parseInt(ordem, 10) <= 300 || base.substring(0, 3) != "000");
} //isCnpj



/**
 * Testa se a String pCpfCnpj fornecida � um CPF ou CNPJ v�lido.
 * Se a String tiver uma quantidade de d�gitos igual ou inferior
 * a 11, valida como CPF. Se for maior que 11, valida como CNPJ.
 * Qualquer formata��o que n�o seja algarismos � desconsiderada.
 * @param String pCpfCnpj
 *      String fornecida para ser testada.
 * @return <code>true</code> se a String fornecida for um CPF ou CNPJ v�lido.
 */
function isCpfCnpj(pCpfCnpj)
{
	var numero = pCpfCnpj.replace(/\D/g, "");
	var ehValido = false;
	
	if (numero.length > NUM_DIGITOS_CPF) {
		ehValido = isCnpj(pCpfCnpj);
		if (!ehValido)
			alert("CNPJ invalido.");
	}
	else {
		ehValido = isCpf(pCpfCnpj);
		if (!ehValido)
			alert("CPF invalido.");
	}
	return ehValido;
} //isCpfCnpj


var timerform
var speed=1000
function dotimer()
{
	today=new Date()
	slutsec=today.getSeconds();
	slutmin=today.getMinutes();
	sluttim=today.getHours();
	sluta=(slutsec) + 60 * (slutmin) + 3600 * (sluttim);
	diff=sluta - starta;
	tim=Math.floor(diff / 3600);
	min=Math.floor((diff / 3600 - tim) * 60);
	sek=Math.round((((diff / 3600 - tim) * 60) - min) * 60);
	document.timerform.timer.value=tim + ":";
	if(min<10) { 
		document.timerform.timer.value+="0";
	}
	document.timerform.timer.value+=min + ":";
	if(sek<10) {
		document.timerform.timer.value+="0";
	}
	document.timerform.timer.value+=sek;
	window.setTimeout("dotimer()",speed);
}

function Timer()
{
	today=new Date()
	startsek=today.getSeconds()
	startmin=today.getMinutes()
	starttim=today.getHours()
	starta=(startsek) + 60 * (startmin) + 3600 * (starttim)
	document.write("<form id=\"timerform\" name=\"timerform\">")
	document.write("Sua sessao vai expirar em ")
	document.write("<input id=\"timer\" name=\"timer\" size=\"7\">")
	document.write(" minutos")
	document.write("</form>")
	dotimer()
}



